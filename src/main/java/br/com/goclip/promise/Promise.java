package br.com.goclip.promise;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.Function;

/**
 * Created by paulo on 25/11/15.
 */
public class Promise<T> {

    private Deferred<T> deferred;

    public Promise(Deferred<T> deferred) {
        this.deferred = deferred;
    }

    public <U> Promise<U> then(Function<T, U> func) {
        CompletableFuture<U> completableFuture = deferred.completableFuture
                .thenComposeAsync((T previousResult) ->
                        CompletableFuture.supplyAsync(() -> func.apply(previousResult)));

        return new Deferred<>(completableFuture).promise();
    }

    public <U> Promise<U> chain(Function<T, Promise<U>> func) {
        CompletableFuture<U> completableFuture = deferred.completableFuture
                .thenComposeAsync((T previousResult) -> func.apply(previousResult).deferred.completableFuture);

        return new Deferred<>(completableFuture).promise();
    }

    public <X> Promise<X> error(Function<Throwable, X> supplier) {
        Throwable[] t = new Throwable[1];
        //This code converts the specialized CompletableFuture exception method into a generic one.
        //In other words, map exception handling as a normal Promise handling
        CompletableFuture<T> exceptionally = deferred.completableFuture
                .exceptionally((e) -> {
                    //Unwrapping in case of CompletionException: keeps clean Exception handling
                    if (e instanceof CompletionException) {
                        t[0] = e.getCause();
                    } else {
                        t[0] = e;
                    }
                    return null;
                });

        //t is now in closure. So t[0] will hold the value of the exception caught on the later block
        CompletableFuture<X> xCompletableFuture = exceptionally
                .thenComposeAsync((T ignored) -> CompletableFuture.supplyAsync(() -> supplier.apply(t[0])));

        return new Deferred<X>(xCompletableFuture).promise();
    }
}
