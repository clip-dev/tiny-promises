# Tiny promises

> Tiny Deferred/Promise implementation around CompletableFuture, that aims to follow the Promise/A+ specification where
 possible.

# Usage

## Deferred

Create a new ``Deferred`` instance:

`
    Deferred<> def = new Deferred<String>();
`

Where ``String`` is the expected ``resolve`` value.

To resolve:

`
    def.resolve("hello!");
`

To reject:

`
    def.reject(new Exception());
`

## Promises

You can chain ``Promise`` together by using ``then()``. Passing a function that returns a promise will first
resolve the inner promise, and resolve the outer promise with the resolved value. This is the expected behaviour
Promises/A+ specification.
